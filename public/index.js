// METAPHORS
const metaphors = document.getElementsByClassName("metaphor");
const metaphor_hovers = document.getElementsByClassName("metaphor-hover");

for (const metaphor_hover of metaphor_hovers) {
    metaphor_hover.onmouseenter = () => {
        metaphor_hover.classList.add("hover");
        for (const metaphor of metaphors) {
            metaphor.classList.add("hover");
        }
    };

    metaphor_hover.onmouseleave = () => {
        metaphor_hover.classList.remove("hover");
        for (const metaphor of metaphors) {
            metaphor.classList.remove("hover");
        }
    };
}


// IMAGERY
const imageries = document.getElementsByClassName("imagery");
const imagery_hovers = document.getElementsByClassName("imagery-hover");

for (const imagery_hover of imagery_hovers) {
    imagery_hover.onmouseenter = () => {
        imagery_hover.classList.add("hover");
        for (const imagery of imageries) {
            imagery.classList.add("hover");
        }
    };

    imagery_hover.onmouseleave = () => {
        imagery_hover.classList.remove("hover");
        for (const imagery of imageries) {
            imagery.classList.remove("hover");
        }
    };
}


// REPETITION
const repetitions = document.getElementsByClassName("repetition");
const repetition_hovers = document.getElementsByClassName("repetition-hover");

for (const repetition_hover of repetition_hovers) {
    repetition_hover.onmouseenter = () => {
        repetition_hover.classList.add("hover");
        for (const repetition of repetitions) {
            repetition.classList.add("hover");
        }
    };

    repetition_hover.onmouseleave = () => {
        repetition_hover.classList.remove("hover");
        for (const repetition of repetitions) {
            repetition.classList.remove("hover");
        }
    };
}


// ANALOGIES
const analogies = document.getElementsByClassName("analogy");
const analogy_hovers = document.getElementsByClassName("analogy-hover");

for (const analogy_hover of analogy_hovers) {
    analogy_hover.onmouseenter = () => {
        analogy_hover.classList.add("hover");
        for (const analogy of analogies) {
            analogy.classList.add("hover");
        }
    };

    analogy_hover.onmouseleave = () => {
        analogy_hover.classList.remove("hover");
        for (const analogy of analogies) {
            analogy.classList.remove("hover");
        }
    };
}


// OXYMORONS
const oxymorons = document.getElementsByClassName("oxymoron");
const oxymoron_hovers = document.getElementsByClassName("oxymoron-hover");

for (const oxymoron_hover of oxymoron_hovers) {
    oxymoron_hover.onmouseenter = () => {
        oxymoron_hover.classList.add("hover");
        for (const oxymoron of oxymorons) {
            oxymoron.classList.add("hover");
        }
    };

    oxymoron_hover.onmouseleave = () => {
        oxymoron_hover.classList.remove("hover");
        for (const oxymoron of oxymorons) {
            oxymoron.classList.remove("hover");
        }
    };
}


// SIMILES
const similes = document.getElementsByClassName("simile");
const simile_hovers = document.getElementsByClassName("simile-hover");

for (const simile_hover of simile_hovers) {
    simile_hover.onmouseenter = () => {
        simile_hover.classList.add("hover");
        for (const simile of similes) {
            simile.classList.add("hover");
        }
    };

    simile_hover.onmouseleave = () => {
        simile_hover.classList.remove("hover");
        for (const simile of similes) {
            simile.classList.remove("hover");
        }
    };
}

const annotation = document.getElementById("longboi");
window.onscroll = () => {
    window.requestAnimationFrame(() => {
        const rect = annotation.getBoundingClientRect();
        if (rect.top < 0) {
            annotation.classList.add("annotations-lower");
        }
        if (rect.top > screen.height - 300) {
            annotation.classList.remove("annotations-lower");
        }
    });
}